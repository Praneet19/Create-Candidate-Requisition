import React, { useState } from "react";

import DashboardLayout from "../../containers/Home/DashboardLayout";

export default function Home() {
  return (
    <>
      <DashboardLayout />
    </>
  );
}
