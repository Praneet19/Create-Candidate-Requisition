import React from "react";
import Sider from "../../components/Sider";

function SideView() {
  return (
    <>
      <Sider />
    </>
  );
}

export default SideView;
