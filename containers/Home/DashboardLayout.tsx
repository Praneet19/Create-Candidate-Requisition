import React, { useState } from "react";

import { Box, Container, Flex, Spacer } from "@chakra-ui/react";

import FormContainer from "../../containers/Home/FormContainer";
import SideView from "../../containers/Home/SideView";
import Head from "../../components/Head";
import ContextState from "../../ContextAPI/ContextState";

function DashboardLayout(props) {
  const [initialValues, setInitialValues] = React.useState({
    RequestTitle: "",
    owner: "",
    hiringmanager: "",
    NOO: "",
    urgency: "",
    status: "",
    jobtitle: "",
    jobDes: "",
    jobLoc: "",
    InterviewMod: "",
    InterviewDur: "",
    InterviewLan: "",
  });

  const [tabIndex, setTabIndex] = React.useState(0);

  function next() {
    setTabIndex(tabIndex + 1);
  }

  function previous() {
    setTabIndex(tabIndex - 1);
  }

  return (
    <>
      <ContextState>
        <Container bg="#F8F9FB" maxWidth="1440px">
          <Container maxWidth="1250px" bg="#F8F9FB">
            <Head tabIndex={tabIndex} />
            <Flex marginTop={7}>
              <Box
                bg="#F8F9FB"
                width="100%"
                p="3"
                textAlign="center"
                rounded="lg"
                color="gray.400"
                marginRight={4}
              >
                <FormContainer
                  next={next}
                  tabIndex={tabIndex}
                  previous={previous}
                  initialValues={initialValues}
                  setInitialValues={setInitialValues}
                />
              </Box>
              <Spacer />
              <Box
                p="2"
                bg="#F8F9FB"
                margin={4}
                w={"700px"}
                boxShadow=" -24px -24px 34px #FFFFFF, 24px 24px 34px rgba(128, 135, 164, 0.13)"
                h={"560px"}
                borderRadius="11px"
                position="relative"
              >
                <SideView />
              </Box>
            </Flex>
          </Container>
        </Container>
      </ContextState>
    </>
  );
}

export default DashboardLayout;
