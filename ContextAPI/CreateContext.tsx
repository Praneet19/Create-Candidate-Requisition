import { createContext } from "react";

const CreateContext = createContext<any | undefined>(undefined);

export default CreateContext;
