import React, { useState } from "react";
import CreateContext from "./CreateContext";

function ContextState(props) {
  //  const [state, setState] = useState("");

  const [state, setState] = useState({
    RequestTitle: "",
    owner: "",
    NOO: "",
    urgency: "",
  });

  return (
    <CreateContext.Provider value={{ state, setState }}>
      {props.children}
    </CreateContext.Provider>
  );
}

export default ContextState;
