import React, { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import {
  Box,
  SimpleGrid,
  FormLabel,
  Input,
  Flex,
  Button,
  Spacer,
} from "@chakra-ui/react";

function JobDetails(props) {
  const MultiformSchema = Yup.object({
    jobtitle: Yup.string().required("Job is Required"),
    jobDes: Yup.string().required("Job Destination is Required"),
    jobLoc: Yup.string().required("Job Location is Required"),
  });

  const formik = useFormik({
    initialValues: props.initialValues,
    validationSchema: MultiformSchema,
    onSubmit: (values) => {
      props.next();
      props.setInitialValues(values);
      console.log(values);
    },
  });
  return (
    <>
      <form onSubmit={formik.handleSubmit}>
        <SimpleGrid columns={1} spacing={2} alignItems="stretch">
          <Box paddingBottom={2}>
            <FormLabel>Job Title</FormLabel>
            <Input
              name="jobtitle"
              size="lg"
              bg="white"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.jobtitle}
            />
            {formik.errors.jobtitle && formik.touched.jobtitle ? (
              <p style={{ color: "red", textAlign: "left" }}>
                {formik.errors.jobtitle}
              </p>
            ) : null}
          </Box>
          <Box paddingBottom={2}>
            <FormLabel>Job Description</FormLabel>
            <Input
              name="jobDes"
              size="lg"
              bg="white"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.jobDes}
            />
            {formik.errors.jobDes && formik.touched.jobDes ? (
              <p style={{ color: "red", textAlign: "left" }}>
                {formik.errors.jobDes}
              </p>
            ) : null}
          </Box>
          <Box paddingBottom={5}>
            <FormLabel>Job Location</FormLabel>
            <Input
              name="jobLoc"
              size="lg"
              bg="white"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.jobLoc}
            />
            {formik.errors.jobLoc && formik.touched.jobLoc ? (
              <p style={{ color: "red", textAlign: "left" }}>
                {formik.errors.jobLoc}
              </p>
            ) : null}
          </Box>
        </SimpleGrid>
        <Flex justify="flex-end" alignItems="flex-end" marginTop={12}>
          <Box>
            <Button
              bg="#8087A4"
              borderRadius="5px"
              padding="16px 50px"
              color="white"
              value={props.tabIndex}
              onClick={props.previous}
              marginTop={8}
              marginBottom={5}
              marginRight={3}
              fontSize={15}
            >
              Previous
            </Button>
          </Box>
          <Box>
            <Button
              borderRadius="5px"
              type="submit"
              padding="16px 55px"
              bg="#E74861"
              color="white"
              value={props.tabIndex}
              marginTop={8}
              marginBottom={5}
              fontSize={15}
              marginRight={1}
            >
              {props.tabIndex == 2 ? "Submit" : "Next"}
            </Button>
          </Box>
        </Flex>
      </form>
    </>
  );
}

export default JobDetails;
