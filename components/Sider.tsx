import {
  Box,
  Container,
  Flex,
  SimpleGrid,
  Spacer,
  Table,
  TableCaption,
  TableContainer,
  Tag,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import React from "react";
import { useContext } from "react";
import CreateContext from "../ContextAPI/CreateContext";

function Sider() {
  const a = useContext(CreateContext);
  return (
    <>
      <Container>
        <Flex>
          <Box fontSize={15}>
            <i>Draft </i>
          </Box>
          <Box
            position="absolute"
            right={0}
            top={0}
            bg={"#e74861"}
            color={"white"}
            padding="5px 37px"
            borderTopRightRadius="11px"
            fontSize="12"
          >
            Preview
          </Box>
        </Flex>

        <Flex
          bg="#37446E"
          marginTop="45px"
          borderRadius={"8px"}
          boxShadow="xl"
          padding="16px 23px"
        >
          <Box
            alignItems="flex-start"
            justifyContent="flex-start"
            color="white"
            padding={1}
          >
            {a.state.RequestTitle}
          </Box>
          <Spacer />
          <Flex alignItems={"center"} justifyItems={"center"}>
            <Box
              color="#F8F8F8"
              fontFamily="Noto Sans"
              fontStyle="normal"
              fontWeight="200"
              fontSize="10px"
              lineHeight="14px"
              textAlign="center"
              letterSpacing="0.05em"
              textTransform="uppercase"
            >
              OPENINGS:
            </Box>
            <Spacer />
            <Box
              color="white"
              paddingLeft={0.5}
              fontFamily="Noto Sans"
              fontStyle="normal"
              fontWeight="600"
              fontSize="15px"
              lineHeight="20px"
              textAlign="center"
              textTransform="capitalize"
            >
              {a.state.NOO}
            </Box>
          </Flex>
        </Flex>

        <Box bg="white" marginTop={"10"} borderRadius="10px">
          <TableContainer>
            <Table variant="unstyled">
              <Thead fontSize={4}>
                <Tr>
                  <Th color="#7a7a7a">REQUISITIONS DETAILS</Th>
                </Tr>
              </Thead>
            </Table>
          </TableContainer>

          <TableContainer>
            <Table variant="unstyled">
              <Thead padding="1px" fontSize={10}>
                <Tr>
                  <Td paddingBottom={0} color="#b9b9b9">
                    OWNER
                  </Td>
                  <Td color="#b9b9b9" paddingBottom={0}>
                    URGENCY
                  </Td>
                  <Td color="#b9b9b9" paddingBottom={0}>
                    EMPLOYMENT TYPE
                  </Td>
                </Tr>
              </Thead>
              <Tbody fontSize={12}>
                <Tr>
                  <Td color="#7a7a7a" paddingTop={0}>
                    <b>
                      <i>{a.state.owner}</i>
                    </b>
                  </Td>
                  <Td color="#7a7a7a" paddingTop={0}>
                    <b>{a.state.urgency}</b>
                  </Td>
                  <Td color="#7a7a7a" paddingTop={0}>
                    <b>Permanent</b>
                  </Td>
                </Tr>
              </Tbody>
            </Table>
          </TableContainer>

          <TableContainer>
            <Table variant="unstyled">
              <Thead fontSize={12}>
                <Tr>
                  <Td color="#b9b9b9" paddingBottom={0}>
                    Gender Preference
                  </Td>
                </Tr>
              </Thead>
              <Tbody fontSize={14}>
                <Tr>
                  <Td color="#7a7a7a" paddingTop={0}>
                    <b>No Preference</b>
                  </Td>
                </Tr>
              </Tbody>
            </Table>
          </TableContainer>
        </Box>
      </Container>
    </>
  );
}

export default Sider;
