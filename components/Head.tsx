import { Box, Container, Flex, Heading } from "@chakra-ui/react";
import React from "react";

const Head = (props) => {
  return (
    <>
      <Box
        //fontFamily="Sora"
        fontStyle="normal"
        fontWeight="700"
        fontSize="28px"
        lineHeight="18px"
        letterSpacing="0.02em"
        textTransform="capitalize"
        paddingBottom={2}
        paddingTop={8}
        color="#00162E"
        textAlign="initial"
        paddingLeft={3}
      >
        <b>Create Candidate Requisition</b>
      </Box>
      <Container
        borderBottom={"1px solid #D6DEFF"}
        maxWidth={"1440px"}
        marginBottom={"2vh"}
      >
        <Flex w={"580px"} alignItems={"left"} justifyItems={"left"}>
          <Box
            font-family="Noto Sans JP"
            font-style="normal"
            font-weight="400"
            font-size="15px"
            line-height="22px"
            letter-spacing="0.01em"
            w={"100%"}
            h={"74px"}
            textAlign={"center"}
            paddingTop={"30px"}
            zIndex={9}
            position={"relative"}
            borderBottom={props.tabIndex === 0 ? "solid 0.5px #FF8282" : null}
            color={props.tabIndex === 0 ? "solid black" : "#8087A4"}
          >
            {props.tabIndex === 0 ? (
              <b>Requestition Details</b>
            ) : (
              "Requestition Details"
            )}
          </Box>
          <Box
            font-family="Noto Sans JP"
            font-style="normal"
            font-weight="400"
            font-size="15px"
            line-height="22px"
            letter-spacing="0.01em"
            w={"100%"}
            h={"74px"}
            textAlign={"center"}
            paddingTop={"25px"}
            zIndex={9}
            position={"relative"}
            borderBottom={props.tabIndex === 1 ? "solid 3px #FF8282" : null}
            color={props.tabIndex === 1 ? "black" : "	#8087A4"}
          >
            {props.tabIndex === 1 ? <b> Job Details </b> : "Job Details"}
          </Box>

          <Box
            font-family="Noto Sans JP"
            font-style="normal"
            font-weight="400"
            font-size="15px"
            line-height="22px"
            letter-spacing="0.01em"
            w={"100%"}
            h={"74px"}
            textAlign={"center"}
            paddingTop={"25px"}
            zIndex={9}
            position={"relative"}
            borderBottom={props.tabIndex === 2 ? " 10px #FF8282" : null}
            color={props.tabIndex === 2 ? "black" : "#8087A4"}
          >
            {props.tabIndex === 2 ? (
              <b> Interview Settings </b>
            ) : (
              "Interview Settings"
            )}
          </Box>
        </Flex>
      </Container>
    </>
  );
};

export default Head;
